# Hausarbeit-LaTeX-Einfuehrung

This is an example how to write a document in LaTeX. It includes a template similar to the Word template provided by FH Bielefeld (University of Applied Sciences).

Um dieses Template nutzen zu können, benötigst du LaTeX. Da die meisten von euch wohl Windows benutzen, gibt es hier eine kleine Anleitung, wie du LaTeX einrichtest. MacOS und Linux Nutzer finden Hilfe unter https://www.latex-project.org/get/

# LaTeX unter Windows einrichten

## LaTeX: MiKTeX
Unter Windows benutze ich persönlich MiKTeX: https://miktex.org/download
In der Vergangenheit ist es vorgekommen, dass die 64-Bit-Version nicht alle Pakete enthalten hat, die in der 32-Bit-Version sind. Wenn du daher auf Nummer Sicher gehen willst, nimm am besten die 32-Bit-Version, auch wenn die unter "All Downloads" versteckt ist.

## Editor: Texmaker
Natürlich kannst du auch mit jedem anderen Editor arbeiten, aber wenn du einen wählst, der für LaTeX ausgerichtet ist, hast du ein paar Vorteile, zum Beispiel kann Texmaker dir das Grundgerüst für eine Tabelle generieren, sodass du die Befehle nicht auswendig kennen musst. Du kannst ihn hier herunterladen: https://www.xm1math.net/texmaker/

Anschließend solltest du unter "Optionen -> Texmaker konfigurieren" in der Zeile Bib(la)tex "biber %" eintragen. Das benötigst du später um die Quellenangaben verarbeiten zu können, da ich im Template biber-Befehle nutze.

# Los geht's!
Jetzt kannst du diese Beispielhausarbeit herunterladen. Entweder du nutzt dazu den Download-Knopf oder du gewöhnst dir gleich an, git zu benutzen und machst git clone.

Zunächst kannst du dir das PDF angucken. Dieses PDF werden wir gleich aus den tex-Dateien bauen.

Dann öffnest du die Datei Hausarbeit.tex und führst PDFLaTeX aus (in Texmaker kannst du dies mit F6 machen). Vermutlich wirst du beim ersten Mal aufgefordert werden, Pakete zu installiere, die im Template genutzt werden. Tue dies bitte.

Heraus kommt eine PDF, die aber noch keine Literaturquellen kennt und auch das Inhaltsverzeichnis wird nach der ersten Durchführung noch leer sein. Das ist ok. LaTeX notiert sich beim ersten Durchgang, welche Kapitel er aufbaut, ab dem zweiten Durchgang füllt er dann das Inhaltsverzeichnis mit diesen Notizen. Daher kann es auch später immer mal wieder sein, dass du 2x bauen musst, damit das Ergebnis deinen Erwartungen entspricht. Auch Verweise auf Bilder oder Kapitel sind beim ersten bauen evtl. noch unbekannt.

Damit LaTeX das Literaturverzeichnis aufbauen kann, nutzen wir biber. Evtl. musst du dies noch in deinem Editor konfigurieren (siehe Editor: Texmaker). Im Texmaker kannst du dies mit F11 starten. Nach erfolgreicher Ausführung kennt LaTeX nun deine Quellen. Dieses Schritt musst du nur wiederholen, wenn du die Bib-Datei änderst.

Wenn du anschließend noch (2x) PDFLaTeX ausführst, solltest du genau das PDF erhalten, das hier hochgeladen ist.

# git
Dies ist ein Git-Repository. Ich empfehle dir, dir selbst ein (privates) Git Repository anzulegen, zum Beispiel hier auf GitLab. Immer wenn du an deiner Hausarbeit gearbeitet hast, solltest du die Änderungen hochladen. So kannst du auf verschiedene Versionen zurück greifen, wenn du zum Beispiel doch einen Teil eines gelöschten Kapitels zurück haben möchtest. Außerdem ist die Arbeit so nicht nur auf deinem eigenen Rechner gespeichert und du kannst auf sie zugreifen, auch wenn deine Festplatte kaputt geht oder dein Laptop geklaut wird.

## Git installieren
Instaliere dir Git von dieser Webseite: https://git-scm.com/ Du bekommst automatisch eine Konsole, in der du git bedienen kannst. Außerdem wird eine (sehr rudimentäre) GUI mitgeliefert. Die Konsole ist eigentlich alles was du brauchst, sollten wir aber Knöpfe lieber sein, dann findest du auf der Webseite auch noch einige andere Programme mit GUI zur Auswahl.

## Git-Basics
Auf der genannten Seite https://git-scm.com/ findest du eine ausführliche Dokumentation und sogar ein ganzes Buch über git. Für den Anfang reichen für dich aber die folgenden Befehle aus:
- git clone - damit kannst du dieses Repository oder dein eigenes, was du auf Gitlab.com erstellt hast, auf deinen Rechner kopieren
- git pull - Die aktuelle Version vom Server auf deinen Rechner ziehen.
- git status - Anzeigen, welche Dateien du auf deinem Rechner verändert hast.
- git add Dateiname - Geänderte Dateien zum Hochladen markieren (statt einzelner Dateien kannst du auch einen * für alle Dateien nehmen).
- git commit -m "Kommentar" - Alle markierten Dateien in einem Commit zusammen fassen und diesem eine Beschreibung geben.
- git push - Den/die Commits auf den Server schieben.

Es gibt noch ein paar mehr Befehle, aber solange du alleine an deiner Hausarbeit arbeitest, werden diese ausreichen.